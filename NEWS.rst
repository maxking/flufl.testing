===============
 flufl.testing
===============

Copyright (C) 2016 Barry A. Warsaw


0.7 (2016-12-14)
================
* Fix minor typo.

0.6 (2016-12-14)
================
* Be sure to declare the namespace package in the setup.py.

0.5 (2016-12-02)
================
* Fix namespace package compatibility.

0.4 (2016-11-30)
================
* More fixes and documentation updates.

0.3 (2016-11-29)
================
* Rename the ``unittest.cfg`` section to ``[flufl.testing]``.
* Improve the documentation.

0.2 (2016-11-28)
================
* Re-enable Python 3.4.
* Update README.

0.1 (2016-11-17)
================
* Initial release.
